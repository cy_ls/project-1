import { http } from "./index"
import { Message } from 'element-ui'
//获取菜单权限列表
export function getMenus() {
    return http('menus', 'GET').then(res => {
        return res.data
    })
}
//获取用户列表
export function getUsers(params) {
    return http('users', 'GET', {}, params).then(res => {
        return res.data
    })
}
//添加用户
export function addUsers(addForm) {
    return http('users', 'post', addForm).then(res => {
        return res.data
    })
}
//修改用户状态
export function updateUserState(uId, type) {
    return http(`users/${uId}/state/${type}`, 'put').then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//编辑用户提交
export function editUser(obj) {
    return http(`users/${obj.id}`, 'PUT', {
        email: obj.email,
        mobile: obj.mobile
    }).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })

}
//删除用户
export function deleteUser(id) {
    return http(`users/${id}`, 'DELETE').then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data

    })
}
//根据id查询角色信息
export function getUsersInfo(id) {
    return http(`users/${id}`, 'GET').then(res => {
        return res.data
    })
}
//获取角色列表哦
export function getRolesLists() {
    return http('roles').then(res => {
        return res.data
    })
}
//分配用户角色
export function handledb(id, rid) {
    return http(`users/${id}/role`, 'PUT', {
        id,
        rid
    }).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//添加角色
export function addRoles(data) {
    return http('roles', 'POSt', data).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//删除角色权限
export function deleteRights(roleId, rightId) {
    return http(`roles/${roleId}/rights/${rightId}`, 'DELETE').then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//编辑提交角色
export function edSubuser(obj) {
    return http(`roles/${obj.id}`, 'PUT', {
        roleName: obj.roleName,
        roleDesc: obj.roleDesc
    }).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//删除角色
export function deleteRole(id) {
    return http(`roles/${id}`, 'DELETE').then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data

    })
}
//角色授权
export function distRoles(roleId, rids) {
    return http(`roles/${roleId}/rights`, 'POST', { rids }).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//所有权限列表
export function allDist(types) {
    return http(`rights/${types}`).then(res => {
        return res.data
    })
}
//获取商品列表
export function getGoodsLists(params) {
    return http('goods', 'GET', {}, params).then(res => {
        return res.data
    })
}
//获取商品分类列表
export function getGoodsCategories() {
    return http('categories', 'GET', {}, {
        type: 3
    }).then(res => {
        return res.data
    })
}
//获取商品参数
export function getGoodsAttrs(id, sel) {
    return http(`categories/${id}/attributes`, 'GET', {}, {
        sel
    }).then(res => {
        return res.data
    })
}
//添加商品
export function addGoods(data) {
    return http('goods', 'POST', data).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//根据id查询商品信息
export function getGoodsDetails(id) {
    return http(`goods/${id}`, 'GET').then(res => {
        return res.data
    })
}
// 编辑商品提交
export function editGoods(data) {
    return http(`goods/${data.goods_id}`, 'PUT', data).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
// 
export function getGoodsCat(params) {
    return http('categories', 'GET', {}, params).then(res => {

        return res.data
    })
}
//添加分类
export function addCategories(params) {
    return http('categories', 'POST', params).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//获取商品分类列表2
export function getGoodsCategories2() {
    return http('categories', 'GET', {}, {
        type: 2
    }).then(res => {
        return res.data
    })
}
//编辑提交参数
export function updateGoodsCatAttrs(id, attrId, data) {
    return http(`categories/${id}/attributes/${attrId}`, 'PUT', data).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//删除参数
export function deleteCatAttrs(id, attrid) {
    return http(`categories/${id}/attributes/${attrid}`, 'DELETE').then(res => {
        return res.data
    })
}
//添加动态属性或参数
export function addOm(id, obj) {
    return http(`categories/${id}/attributes`, 'POST', {
        attr_name: obj.attr_name,
        attr_sel: obj.attr_sel,
        attr_vals: obj.attr_vals
    }).then(res => {
        return res.data
    })
}
//获取折线图数据
export function getReports() {
    return http('reports/type/1').then(res => {
        return res.data
    })
}
//删除分类
export function deleteClassList(id) {
    return http(`categories/${id}`, 'DELETE').then(res => {
        return res.data
    })
}
//编辑提交分类
export function handleClassCat(obj) {
    return http(`categories/${obj.catid}`, 'PUT', {cat_name:obj.catName}).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//添加动态参数或者静态属性
export function addOnmay(id,obj) {
    return http(`categories/${id}/attributes`, 'post', obj).then(res => {
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res.data
    })
}
//订单数据列表
export function getOrders(params) {
    return http('orders', 'GET', {}, params).then(res => {
        return res.data
    })
}