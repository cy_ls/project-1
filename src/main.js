import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import dayjs from'dayjs'
// import '@/assets/reset.css'
import 'reset-css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Myplugins from './plugins'
Vue.use(ElementUI)
Vue.use(Myplugins)
Vue.config.productionTip = false
Vue.filter('dealDate',(val) => {
  return dayjs(val).format('YYYY-MM-DD HH:mm:ss')
})
Vue.filter('dealDate2',(val) => {
  return dayjs(val).format('YYYY-MM-DD HH:mm:ssZ')
})
Vue.filter('jb',(val) => {
  if(val == 0){
    return '一级'
  }else if(val == 1) {
    return '二级'
  }else if(val == 2) {
    return '三级'
  }
})

// Vue.prototype.$http = http
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

